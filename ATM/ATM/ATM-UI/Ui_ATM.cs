﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATMImplementation;    // Import ATMImplementation  so we could link this class with the ATM one

namespace ATMUIImplementation
{
    public partial class Ui_ATM : Form
    {
        public static ATM atm = new ATM();                      // Instantiation of a new ATM class
        public int globalOption;                                // Global param used in ATM class
        public bool WelcomeMenu, ATMAddCurrency, ExtractUDB;    // Global identifiers allowing us to check if we are in Welcome Menu, Add Menu or Extract Menu
        public String Info, option1, option2, option3, option4, // Linkers for UI "labels" and "Rich text box" Texts
            option5, option6, option7, option8, CurrencyTypeText;

        public Ui_ATM()
        {
            InitializeComponent();
        }

        private void Ui_ATM_FormClosed(object sender, FormClosedEventArgs e)
        {
            atm.StopATM();
        }

        /**
         * @fn void SetButtons()
         *
         * @brief               In this method we enable/disable our buttons except the ok and cancel ones          
         *
         * @param btl1          This enable/Disable the 1st uper left button
         * @param btl2          This enable/Disable the 2nd uper left button
         * @param btl3          This enable/Disable the 3rd uper left button
         * @param btl4          This enable/Disable the 4th uper left button
         * @param btr1          This enable/Disable the 1st uper right button
         * @param btr2          This enable/Disable the 2nd uper right button
         * @param btl3          This enable/Disable the 3rd uper right button
         * @param btl4          This enable/Disable the 4th uper right button
         * 
         * @author Vlad
         * @date 25-4-2015
         */
        public void SetButtons(bool btl1, bool btl2, bool btl3, bool btl4,  bool btr1, bool btr2, bool btr3, bool btr4)
        {
            btn_Left_1.Enabled = btl1;
            btn_Left_2.Enabled = btl2;
            btn_Left_3.Enabled = btl3;
            btn_Left_4.Enabled = btl4;
            btn_Right_1.Enabled = btr1;
            btn_Right_2.Enabled = btr2;
            btn_Right_3.Enabled = btr3;
            btn_Right_4.Enabled = btr4;
        }

        /**
         * @fn void RefreshForm()
         *
         * @brief               This method refreshes the text of our UI elements "labels" and "Rich Text box" allowing us to see the changes visually.   
         *
         * @author Vlad
         * @date 25-4-2015
         */
        public void RefreshForm()
        {
            lbl_option_1.Text = option1;
            lbl_option_2.Text = option2;
            lbl_option_3.Text = option3;
            lbl_option_4.Text = option4;
            lbl_option_5.Text = option5;
            lbl_option_6.Text = option6;
            lbl_option_7.Text = option7;
            lbl_option_8.Text = option8;
            Rtb.Text = Info;
        }

        private void Ui_ATM_Load(object sender, EventArgs e)
        {
            lbl_option_1.Text = option1;
            lbl_option_2.Text = option2;
            lbl_option_3.Text = option3;
            lbl_option_4.Text = option4;
            lbl_option_5.Text = option5;
            lbl_option_6.Text = option6;
            lbl_option_7.Text = option7;
            lbl_option_8.Text = option8;
            WelcomeMenu = ATMAddCurrency = ExtractUDB = false;
            Rtb.SelectionAlignment = HorizontalAlignment.Center;
            Rtb.Text = Info;
        }        
        
        private void btn_Left_1_Click(object sender, EventArgs e)
        {
            if (WelcomeMenu)
            {
                WelcomeMenu = false;
                globalOption = 2; // ChoseOption: Sold
            }
            atm.ChoseOption(lbl_option_1.Text, globalOption);
        }

        private void btn_Left_2_Click(object sender, EventArgs e)
        {
            if (ExtractUDB)
            {
                ExtractUDB = false;
                WelcomeMenu = false;
                ATMAddCurrency = false;
                globalOption = 3; // ChoseOption: Extract
            }
            atm.ChoseOption(lbl_option_2.Text, globalOption);
        }

        private void btn_Left_3_Click(object sender, EventArgs e)
        {
            if (ATMAddCurrency)
            {
                WelcomeMenu = false;
                ATMAddCurrency = false;
                globalOption = 4; // ChoseOption: Add Curency
            }
            atm.ChoseOption(lbl_option_3.Text, globalOption);
        }

        private void btn_Left_4_Click(object sender, EventArgs e)
        {
            if (WelcomeMenu)
            {
                WelcomeMenu = false;
                ATMAddCurrency = false;
                globalOption = 0; // ChoseOption: Change Curency
            }
            atm.ChoseOption(lbl_option_4.Text, globalOption);
        }

        private void btn_Right_1_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_5.Text, globalOption);
        }

        private void btn_Right_2_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_6.Text, globalOption);
        }

        private void btn_Right_3_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_7.Text, globalOption);
        }

        private void btn_Right_4_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_8.Text, globalOption);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_8.Text, 1);
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            atm.ChoseOption(lbl_option_8.Text, 1);
        }
    }
}
