﻿namespace ATMUIImplementation
{
    partial class Ui_ATM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Right_1 = new System.Windows.Forms.Button();
            this.btn_Right_2 = new System.Windows.Forms.Button();
            this.btn_Right_3 = new System.Windows.Forms.Button();
            this.btn_Right_4 = new System.Windows.Forms.Button();
            this.btn_Left_1 = new System.Windows.Forms.Button();
            this.btn_Left_2 = new System.Windows.Forms.Button();
            this.btn_Left_3 = new System.Windows.Forms.Button();
            this.btn_Left_4 = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.lbl_option_1 = new System.Windows.Forms.Label();
            this.lbl_option_2 = new System.Windows.Forms.Label();
            this.lbl_option_3 = new System.Windows.Forms.Label();
            this.lbl_option_4 = new System.Windows.Forms.Label();
            this.lbl_option_5 = new System.Windows.Forms.Label();
            this.lbl_option_6 = new System.Windows.Forms.Label();
            this.lbl_option_7 = new System.Windows.Forms.Label();
            this.lbl_option_8 = new System.Windows.Forms.Label();
            this.Rtb = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btn_Right_1
            // 
            this.btn_Right_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Right_1.Location = new System.Drawing.Point(404, 30);
            this.btn_Right_1.Name = "btn_Right_1";
            this.btn_Right_1.Size = new System.Drawing.Size(55, 23);
            this.btn_Right_1.TabIndex = 0;
            this.btn_Right_1.Text = "<";
            this.btn_Right_1.UseVisualStyleBackColor = false;
            this.btn_Right_1.Click += new System.EventHandler(this.btn_Right_1_Click);
            // 
            // btn_Right_2
            // 
            this.btn_Right_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Right_2.Location = new System.Drawing.Point(404, 73);
            this.btn_Right_2.Name = "btn_Right_2";
            this.btn_Right_2.Size = new System.Drawing.Size(55, 23);
            this.btn_Right_2.TabIndex = 1;
            this.btn_Right_2.Text = "<";
            this.btn_Right_2.UseVisualStyleBackColor = false;
            this.btn_Right_2.Click += new System.EventHandler(this.btn_Right_2_Click);
            // 
            // btn_Right_3
            // 
            this.btn_Right_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Right_3.Location = new System.Drawing.Point(404, 119);
            this.btn_Right_3.Name = "btn_Right_3";
            this.btn_Right_3.Size = new System.Drawing.Size(55, 23);
            this.btn_Right_3.TabIndex = 2;
            this.btn_Right_3.Text = "<";
            this.btn_Right_3.UseVisualStyleBackColor = false;
            this.btn_Right_3.Click += new System.EventHandler(this.btn_Right_3_Click);
            // 
            // btn_Right_4
            // 
            this.btn_Right_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Right_4.Location = new System.Drawing.Point(404, 164);
            this.btn_Right_4.Name = "btn_Right_4";
            this.btn_Right_4.Size = new System.Drawing.Size(55, 23);
            this.btn_Right_4.TabIndex = 3;
            this.btn_Right_4.Text = "<";
            this.btn_Right_4.UseVisualStyleBackColor = false;
            this.btn_Right_4.Click += new System.EventHandler(this.btn_Right_4_Click);
            // 
            // btn_Left_1
            // 
            this.btn_Left_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Left_1.Location = new System.Drawing.Point(23, 30);
            this.btn_Left_1.Name = "btn_Left_1";
            this.btn_Left_1.Size = new System.Drawing.Size(52, 23);
            this.btn_Left_1.TabIndex = 4;
            this.btn_Left_1.Text = ">";
            this.btn_Left_1.UseVisualStyleBackColor = false;
            this.btn_Left_1.Click += new System.EventHandler(this.btn_Left_1_Click);
            // 
            // btn_Left_2
            // 
            this.btn_Left_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Left_2.Location = new System.Drawing.Point(23, 73);
            this.btn_Left_2.Name = "btn_Left_2";
            this.btn_Left_2.Size = new System.Drawing.Size(52, 23);
            this.btn_Left_2.TabIndex = 5;
            this.btn_Left_2.Text = ">";
            this.btn_Left_2.UseVisualStyleBackColor = false;
            this.btn_Left_2.Click += new System.EventHandler(this.btn_Left_2_Click);
            // 
            // btn_Left_3
            // 
            this.btn_Left_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Left_3.Location = new System.Drawing.Point(23, 119);
            this.btn_Left_3.Name = "btn_Left_3";
            this.btn_Left_3.Size = new System.Drawing.Size(52, 23);
            this.btn_Left_3.TabIndex = 6;
            this.btn_Left_3.Text = ">";
            this.btn_Left_3.UseVisualStyleBackColor = false;
            this.btn_Left_3.Click += new System.EventHandler(this.btn_Left_3_Click);
            // 
            // btn_Left_4
            // 
            this.btn_Left_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Left_4.Location = new System.Drawing.Point(23, 164);
            this.btn_Left_4.Name = "btn_Left_4";
            this.btn_Left_4.Size = new System.Drawing.Size(52, 23);
            this.btn_Left_4.TabIndex = 7;
            this.btn_Left_4.Text = ">";
            this.btn_Left_4.UseVisualStyleBackColor = false;
            this.btn_Left_4.Click += new System.EventHandler(this.btn_Left_4_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_ok.Location = new System.Drawing.Point(127, 218);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 9;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_cancel.Location = new System.Drawing.Point(255, 218);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 10;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // lbl_option_1
            // 
            this.lbl_option_1.AutoSize = true;
            this.lbl_option_1.BackColor = System.Drawing.Color.White;
            this.lbl_option_1.Location = new System.Drawing.Point(108, 35);
            this.lbl_option_1.Name = "lbl_option_1";
            this.lbl_option_1.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_1.TabIndex = 11;
            this.lbl_option_1.Text = "label1";
            // 
            // lbl_option_2
            // 
            this.lbl_option_2.AutoSize = true;
            this.lbl_option_2.BackColor = System.Drawing.Color.White;
            this.lbl_option_2.Location = new System.Drawing.Point(108, 78);
            this.lbl_option_2.Name = "lbl_option_2";
            this.lbl_option_2.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_2.TabIndex = 12;
            this.lbl_option_2.Text = "label2";
            // 
            // lbl_option_3
            // 
            this.lbl_option_3.AutoSize = true;
            this.lbl_option_3.BackColor = System.Drawing.Color.White;
            this.lbl_option_3.Location = new System.Drawing.Point(108, 124);
            this.lbl_option_3.Name = "lbl_option_3";
            this.lbl_option_3.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_3.TabIndex = 13;
            this.lbl_option_3.Text = "label3";
            // 
            // lbl_option_4
            // 
            this.lbl_option_4.AutoSize = true;
            this.lbl_option_4.BackColor = System.Drawing.Color.White;
            this.lbl_option_4.Location = new System.Drawing.Point(108, 169);
            this.lbl_option_4.Name = "lbl_option_4";
            this.lbl_option_4.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_4.TabIndex = 14;
            this.lbl_option_4.Text = "label4";
            // 
            // lbl_option_5
            // 
            this.lbl_option_5.AutoSize = true;
            this.lbl_option_5.BackColor = System.Drawing.Color.White;
            this.lbl_option_5.Location = new System.Drawing.Point(341, 35);
            this.lbl_option_5.Name = "lbl_option_5";
            this.lbl_option_5.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_5.TabIndex = 15;
            this.lbl_option_5.Text = "label5";
            // 
            // lbl_option_6
            // 
            this.lbl_option_6.AutoSize = true;
            this.lbl_option_6.BackColor = System.Drawing.Color.White;
            this.lbl_option_6.Location = new System.Drawing.Point(341, 78);
            this.lbl_option_6.Name = "lbl_option_6";
            this.lbl_option_6.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_6.TabIndex = 16;
            this.lbl_option_6.Text = "label6";
            // 
            // lbl_option_7
            // 
            this.lbl_option_7.AutoSize = true;
            this.lbl_option_7.BackColor = System.Drawing.Color.White;
            this.lbl_option_7.Location = new System.Drawing.Point(341, 124);
            this.lbl_option_7.Name = "lbl_option_7";
            this.lbl_option_7.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_7.TabIndex = 17;
            this.lbl_option_7.Text = "label7";
            // 
            // lbl_option_8
            // 
            this.lbl_option_8.AutoSize = true;
            this.lbl_option_8.BackColor = System.Drawing.Color.White;
            this.lbl_option_8.Location = new System.Drawing.Point(341, 169);
            this.lbl_option_8.Name = "lbl_option_8";
            this.lbl_option_8.Size = new System.Drawing.Size(35, 13);
            this.lbl_option_8.TabIndex = 18;
            this.lbl_option_8.Text = "label8";
            // 
            // Rtb
            // 
            this.Rtb.BackColor = System.Drawing.Color.White;
            this.Rtb.BulletIndent = 8;
            this.Rtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rtb.Location = new System.Drawing.Point(94, 12);
            this.Rtb.Name = "Rtb";
            this.Rtb.ReadOnly = true;
            this.Rtb.Size = new System.Drawing.Size(292, 186);
            this.Rtb.TabIndex = 19;
            this.Rtb.Text = "";
            // 
            // Ui_ATM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(476, 277);
            this.Controls.Add(this.lbl_option_8);
            this.Controls.Add(this.lbl_option_7);
            this.Controls.Add(this.lbl_option_6);
            this.Controls.Add(this.lbl_option_5);
            this.Controls.Add(this.lbl_option_4);
            this.Controls.Add(this.lbl_option_3);
            this.Controls.Add(this.lbl_option_2);
            this.Controls.Add(this.lbl_option_1);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.btn_Left_4);
            this.Controls.Add(this.btn_Left_3);
            this.Controls.Add(this.btn_Left_2);
            this.Controls.Add(this.btn_Left_1);
            this.Controls.Add(this.btn_Right_4);
            this.Controls.Add(this.btn_Right_3);
            this.Controls.Add(this.btn_Right_2);
            this.Controls.Add(this.btn_Right_1);
            this.Controls.Add(this.Rtb);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Ui_ATM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ui_ATM";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Ui_ATM_FormClosed);
            this.Load += new System.EventHandler(this.Ui_ATM_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Right_1;
        private System.Windows.Forms.Button btn_Right_2;
        private System.Windows.Forms.Button btn_Right_3;
        private System.Windows.Forms.Button btn_Right_4;
        private System.Windows.Forms.Button btn_Left_1;
        private System.Windows.Forms.Button btn_Left_2;
        private System.Windows.Forms.Button btn_Left_3;
        private System.Windows.Forms.Button btn_Left_4;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label lbl_option_1;
        private System.Windows.Forms.Label lbl_option_2;
        private System.Windows.Forms.Label lbl_option_3;
        private System.Windows.Forms.Label lbl_option_4;
        private System.Windows.Forms.Label lbl_option_5;
        private System.Windows.Forms.Label lbl_option_6;
        private System.Windows.Forms.Label lbl_option_7;
        private System.Windows.Forms.Label lbl_option_8;
        private System.Windows.Forms.RichTextBox Rtb;
    }
}