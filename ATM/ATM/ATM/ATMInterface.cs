﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* NOTE:
 * Interfetele members cannot have access modifiers and are public by deafault
 * Cannot have implementations just declarations 
 */
namespace IATM
{
    interface IATMImpl
    {
        void OpenATM();
        void StopATM();
    }

    interface IUnitateDeBani : IATMImpl
    {
        void ModifyUDBName(string name);
        void AddUDB(int value);
        void ExtractUDB(int value);
    }
}
