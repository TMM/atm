﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IATM;                 // Importing IATM namespace so class ATM could inherit  with IUnitateDeBani
using ATMUIImplementation;  // Import ATMUIImplementation namespace so we can link the ui with this ATM class
using System.Windows.Forms;

namespace ATMImplementation
{
    public class ATM : IUnitateDeBani
    {
        private static Ui_ATM frm = new Ui_ATM();   // Instantiation of a new UI_ATM form where we will update later on
        public string _CurrencyTypeText;            // string that keeps track of curency type £, $ etc
        private int CurrencyValue = 10;             // The initial sold (brute value) this can be linked to a database table value

        /**
         * @fn void WelcomeOption()
         *
         * @brief           This fn is the "main screen" after you chose the ATM name. Basicly this is the main manu of this ATM application
         *                  In other words this is the visual setup for UI that any customer will see and can acces by clicking on OK or Cancel buttons
         *
         * @author Vlad
         * @date 25-4-2015
         */
        private void WelcomeOption()
        {
            frm.Info = "Hello\n\n\n What do you want to do next?";
            frm.option1 = "Sold";
            frm.option2 = "Extract " + _CurrencyTypeText;
            frm.option3 = "Add " + _CurrencyTypeText;
            frm.option4 = "Change Currency Type";
            frm.option5 = "";
            frm.option6 = "";
            frm.option7 = "";
            frm.option8 = "";
            frm.SetButtons(true, true, true, true, false, false, false, false);
            frm.WelcomeMenu = true;
            frm.ATMAddCurrency = true;
            frm.ExtractUDB = true;
            frm.RefreshForm(); 
        }

        /**
         * @fn void SoldOption()
         *
         * @brief           Visual setup for UI, that allow you to see your current sold
         *
         * @author Vlad
         * @date 25-4-2015
         */
        private void SoldOption()
        {
            frm.Info = "\n\n\n\n You have " + CurrencyValue.ToString() + " " + _CurrencyTypeText + " in your account\n Please hit 'ok' or 'Cancel' button to return back to the Welcome Screen";
            frm.option1 = "";
            frm.option2 = "";
            frm.option3 = "";
            frm.option4 = "";
            frm.option5 = "";
            frm.option6 = "";
            frm.option7 = "";
            frm.option8 = "";
            frm.SetButtons(false, false, false, false, false, false, false, false);
            frm.RefreshForm(); 
        }

        /**
         * @fn void ATMAddCurrency()
         *
         * @brief           Visual setup for UI, that gives you few option regarding your amount of currency you want to add to your account
         *
         * @author Vlad
         * @date 25-4-2015
         */
        private void ATMAddCurrency()
        {
            frm.Info = "\n\n\n\n You have " + CurrencyValue.ToString() + " " + _CurrencyTypeText + "\nchose how much money\n you want to add?";
            frm.option1 = "10";
            frm.option2 = "20";
            frm.option3 = "50";
            frm.option4 = "100";
            frm.option5 = "200";
            frm.option6 = "500";
            frm.option7 = "1000";
            frm.option8 = "2000";
            frm.SetButtons(true, true, true, true, true, true, true, true);
            frm.RefreshForm();
            frm.globalOption = 5;
        }

        /**
         * @fn void ExtractOption()
         *
         * @brief           Visual setup for UI, allowing you to extract the amount of currency from predifined options
         *
         * @author Vlad
         * @date 25-4-2015
         */
        private void ExtractOption()
        {
            frm.Info = "\n\n\n\n You have " + CurrencyValue.ToString() + " " + _CurrencyTypeText + "\nchose how much money\n you want to extract?";
            frm.option1 = "10";
            frm.option2 = "20";
            frm.option3 = "50";
            frm.option4 = "100";
            frm.option5 = "200";
            frm.option6 = "500";
            frm.option7 = "1000";
            frm.option8 = "2000";
            frm.SetButtons(true, true, true, true, true, true, true, true);
            frm.RefreshForm();
            frm.globalOption = 6;
        }

        /**
         * @fn void ChoseOption()
         *
         * @brief           This method links some the ui elements with the backend part of this code.
         *
         * @param test      Global string that were we store the ATM's name and name of the currency
         * @param option    Give us the actual option we want to do the options are
         *                  0 = for selecting the ATM's name
         *                  1 = For selecting the currency we want to work with
         *                  2 = The sold interocation
         *                  3 = The extract currency option
         *                  4 = The add curency option
         *                  5 = used for updating the actual currency calling AddUDB method
         *                  6 = used for updating the actual currency calling ExtractUDB method
         * 
         * @author Vlad
         * @date 25-4-2015
         */
        public void ChoseOption(string test, int option)
        {
            switch (option)
            {
                case 0: //  Select atm name
                    {
                        ModifyUDBName(test);
                        frm.globalOption = 1;
                        frm.SetButtons(true, true, true, true, true, true, true, true);
                        Console.WriteLine("ATM Name Chosen");
                    }
                    break;
                case 1: // Select ATM Currency  Name
                    {
                        if (test == "Ruble")
                            test = "р.";
                        if (test == "Euro")
                            test = "€";
                        if (test == "Lek")
                            test = "L";
                        if (test == "Pound")
                            test = "£";
                        if (test == "Dinar")
                            test = "د.ج";
                        if (test == "Dollar")
                            test = "$";
                        if (test == "Peos")
                            test = "$";
                        if (test == "Lev")
                            test = "лв";
                        _CurrencyTypeText = test;
                        Console.WriteLine("ATM Currency Name Chosen");
                        WelcomeOption();
                    }
                    break;
                case 2: // Sold
                    {
                        Console.WriteLine("ATM SoldOption method is called");
                        SoldOption();
                    } break;
                case 3: // Extract
                    {
                        Console.WriteLine("ATM Extract method is called");
                        ExtractOption();
                    } break;
                case 4: // ATMAddCurrency
                    {
                        Console.WriteLine("ATM ATMAddCurrency method is called");
                        ATMAddCurrency();
                    } break;
                case 5: // Call AddUDB method
                    {
                        AddUDB(int.Parse(test));
                        frm.globalOption = 1;
                        WelcomeOption();
                    }
                    break;
                case 6: // Call ExtractUDB method
                    {
                        ExtractUDB(int.Parse(test));
                        frm.globalOption = 1;
                        WelcomeOption();
                    }
                    break;

            }
        }

        /**
         * @fn void SelectCurencyType()
         *
         * @brief           Visual setup for UI allowing us to chose the currency we want to work with
         *
         * @author Vlad
         * @date 25-4-2015
         */
        private void SelectCurencyType()
        {
            frm.Info = "Hello\n\n\n Please choose ATM's Currency Type";
            frm.option1 = "Ruble";
            frm.option2 = "Euro";
            frm.option3 = "Lek";
            frm.option4 = "Pound";
            frm.option5 = "Dinar";
            frm.option6 = "Dollar";
            frm.option7 = "Peos";
            frm.option8 = "Lev";
            frm.RefreshForm();
        }

        /**
         * @fn void ModifyUDBName()
         *
         * @brief           In this method we modify the Currency name.
         *
         * @param name      The new currency name is stored here.
         * @author Vlad
         * @date 25-4-2015
         */
        public void ModifyUDBName(string name)
        {
            frm.Text = name;
            SelectCurencyType();
            Console.WriteLine("ATM ModifyUDBName");
        }

        /**
         * @fn void AddUDB()
         *
         * @brief           In this method we add to our account the amount of currency that we want.
         *
         * param value      The actual value that we want to Add to our account.
         * @author Vlad
         * @date 25-4-2015
         */
        public void AddUDB(int value)
        {
            CurrencyValue += value;
            Console.WriteLine("ATM AddUDB");
        }

        /**
         * @fn void ExtractUDB()
         *
         * @brief           The extract method that extract the amount of currency we want.
         *                  Note: if the value is higher that the value that we have it will inform us about it and return to the main screen.
         *
         * param value      The actual value that we want to extract from our account.
         * 
         * @author Vlad
         * @date 25-4-2015
         */
        public void ExtractUDB(int value)
        {
            if (CurrencyValue < value)
            {
                Console.WriteLine("Insuficient fonds!");
                MessageBox.Show("Insuficient fonds!");
                return;
            }
            else
            {
                CurrencyValue -= value;
            }
            Console.WriteLine("ATM ExtractUDB");
        }

        /**
         * @fn void OpenATM()
         *
         * @brief           Visual setup for ATM main screen.
         *                  Note: Here we setup the fix place for our ui (*)
         *
         * @author Vlad
         * @date 25-4-2015
         */
        public void OpenATM()
        {
            Console.WriteLine("ATM UI Opened");
            frm.FormBorderStyle = FormBorderStyle.FixedSingle; // (*)
            frm.Info = "Hello\n\n\n Please choose ATM's Name";
            frm.option1 = "Name 1";
            frm.option2 = "Name 2";
            frm.option3 = "Name 3";
            frm.option4 = "Name 4";
            frm.option5 = "Name 5";
            frm.option6 = "Name 6";
            frm.option7 = "Name 7";
            frm.option8 = "Name 8";
            frm.ShowDialog();
        }

        /**
         * @fn void StopATM()
         *
         * @brief           This method is used to stop the ATM (is not used in this project yet)
         *
         * @author Vlad
         * @date 25-4-2015
         */
        public void StopATM()
        {
            frm.Close();
            Console.WriteLine("ATM UI Closed");
        }
    }
}
